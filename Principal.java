
public class Principal {

	public static void main(String[] args)
	{
		Runtime r = Runtime.getRuntime();
		int n = r.availableProcessors();
		Thread h[] = new Thread[n];
		
		for(int i = 0;i < n;i++)
		{
			h[i] = new Thread(new Hilo(i+1));
			h[i].start();
		}
		
		for(int i = 0;i < n;i++)
		{
			try {
				h[i].join();
			} catch (Exception e) {}
		}
		
		System.out.println("La cuenta es "+Hilo.cont);
	}

}

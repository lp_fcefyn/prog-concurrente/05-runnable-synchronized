
public class Hilo implements Runnable{
	private static Object lock = new Object();
	public static int cont = 0;
	private int id;
	
	public Hilo(int id) 
	{
		this.id = id;
	}
	
	@Override
	public void run() {
		System.out.println("Hola soy el hilo "+this.id);
		synchronized(lock)				// Esto es ineficiente, se esta comportando de manera secuencial + el esfuerzo por usar hilos, 
		{								// es decir es menos eficiente que un programa secuencial en lugar de mas eficiente.
			for(int i = 0;i < 40000;i++)// cont++ es una seccion critica pues muchos hilos escriben sobre una misma variable estatica.
			{							// ahora con el synchronized se forma una region critica (una seccion critica controlada).		 
				cont++;					// Se pone el for dentro del synchronized y no al reves ya que el despertar a los hilos que estan
			}							// esperando y que uno tome el lock lleva un tiempo, por lo que hay que minimizar la cantidad de
		}								// veces que esto sucede. Si el synchronized estuviese dentro del for, este proceso ocurriria
	}									// 160000 veces en lugar de las 4 veces que sucede con el for dentro del synchronized (pues se cuenta
}										// con 4 hilos)
